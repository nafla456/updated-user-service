package com.orderservice.OrderService.controller;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //localhost:8083/api/order/getOrdersByUserId/1


//this below methode is defined for intercommunication bewtween 2 microservices
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final long id){
        return orderService.getOrdersByUserId(id);
    }
}
