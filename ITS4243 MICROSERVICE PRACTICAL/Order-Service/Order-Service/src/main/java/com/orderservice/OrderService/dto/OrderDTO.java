package com.orderservice.OrderService.dto;

public class OrderDTO {
    private String Id;
    private String orderId;
    private String userId;

    public OrderDTO() {
    }

    public OrderDTO(String id, String orderId, String userId) {
        Id = id;
        this.orderId = orderId;
        this.userId = userId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
