package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);


    @Autowired
    private OrderRepository repository;

    public List<OrderDTO> getOrdersByUserId(Long Id){

            List<OrderDTO> orders = null;
        try {
                    orders =repository.findOrdersByUserId(Id.toString())
                    .stream()
                    .map(order -> new OrderDTO( /*convert order object into OrderDTO*/
                            order.getId().toString(),
                            order.getOrderId(),
                            order.getUserId()
                    )).collect(Collectors.toList());
        }
        catch (Exception e){
            LOGGER.warn("Exception in GetOrdersByUserId");

        }
        return orders;

    }
}
