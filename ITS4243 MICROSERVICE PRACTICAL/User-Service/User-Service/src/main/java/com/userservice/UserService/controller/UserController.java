package com.userservice.UserService.controller;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    //localhost:8080/api/user/getAll
    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return Arrays.asList(
                new UserDTO("1","Thushan","24"),
                new UserDTO("2","Nishani","22")
        );

    }
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final long id){
        return userService.getOrdersByUserId(id);
    }
}

