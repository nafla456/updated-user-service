package com.userservice.UserService.services;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl; //these are the parameters which can see in yml file..parameters are read from here

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate; //API call inbewteen backend

    @Autowired
    private UserRepository repository;

    public List<OrderDTO> getOrdersByUserId(Long Id){
        List<OrderDTO> orders=restTemplate.build().getForObject( // we are creating a object from resttemplate and we are retriving
                // the object from orderserviceBaseUrl
                // then we concatinate rest of the url and ID is passed from here
                //
                orderServiceBaseUrl.concat(orderServiceOrderUrl)
                        .concat("/"+Id),List.class);
        return orders;
    }


    /** ======== This methods is used retrieve all the user data
     *
     */

    public List<UserDTO> getAllusers(){/** In here we are going to retrieve list of users*/
        List<UserDTO>userDTOS =null;
        try {
            userDTOS = repository.findAll()
                    .stream()
                    .map(user-> new UserDTO(
                            user.getId().toString(),
                            user.getName(),
                            user.getAge()

                    )).collect(Collectors.toList());
        }
        catch (Exception e){

        }
        return userDTOS;


    }
}
